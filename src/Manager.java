import java.util.Scanner;

public class Manager {
    static int numberOfCars;
    static int numberOfTrucks;
    static Car[] parkingSpaсe;

    public static void main(String[] args) {

        enterTheNumberOfParkingSpaces();
        instruction();
        commands();


    }

    private static void enterTheNumberOfParkingSpaces() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество парковочных мест для машин:");
        numberOfCars = scanner.nextInt();
        System.out.println("Введите количетво паврковочных мест для грузовиков:");
        numberOfTrucks = scanner.nextInt();
        if (numberOfCars <= 0 | numberOfTrucks <= 0) {
            System.out.println("Нельзя создать пустую парковку");
            enterTheNumberOfParkingSpaces();
        } else {
            Parking parking = new Parking(numberOfCars, numberOfTrucks);
            parkingSpaсe = new Car[parking.getNumersOfCars() + (parking.getNumbersOfTrucks())];
        }
    }

    static void commands() {
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();

        if (command.equals("/s")) {
            creatingParkingSpace();
        }
        if (command.equals("/left")) {
            return;
        }
        commands();
    }


    private static void creatingParkingSpace() {
        int cars = (int) (Math.random() * (numberOfCars) / 2);// новые машины
        int trucks = (int) (Math.random() * (numberOfCars) / 2);// новые грузвовики
        int countOfCars = 0; //сколько удалось впихнуть
        while (cars > 0) {
            for (int i = 0; i <= numberOfCars - 1; i++) {
                if ((parkingSpaсe[i] == null)) {
                    parkingSpaсe[i] = new Car(((int) (Math.random() * 11)) + 1);
                    countOfCars += 1;
                    break;
                }
            }
            cars -= 1;
        }
        int countOfTrucks = 0; //сколько удалось впихнуть
        int trucks1 = trucks;
        while (trucks > 0) {
            for (int i = numberOfCars; i <= numberOfCars + numberOfTrucks - 1; i++) {
                if (parkingSpaсe[i] == null) {
                    int n = ((int) (Math.random() * 11)) + 1;
                    parkingSpaсe[i] = new Car(n);
                    countOfTrucks += 1;
                    break;
                }
            }
            trucks -= 1;
        }

        if (countOfTrucks == 0) {
            while (trucks1 > 0) {
                for (int i = 0; i <= numberOfCars - 1; i++) {
                    if ((parkingSpaсe[i] == null & parkingSpaсe[i + 1] == null)) {
                        int m = ((int) (1 + Math.random() * 10)) + 1;
                        parkingSpaсe[i] = new Car(m);
                        parkingSpaсe[i + 1] = new Car(m);
                        countOfTrucks += 1;
                        break;
                    }
                }
                trucks1 = trucks1 - 1;
            }
        }

        checkingTheNumbersOfMachines();
        cleaningTheParkingLot();
        System.out.println("Новых машин: " + (countOfCars) + "\n" + "Новых грузовиков: " + (countOfTrucks));
    }

    private static void checkingTheNumbersOfMachines() {
        int freeSpaceForCars = 0;
        for (int i = 0; i <= numberOfCars - 1; i++) {
            if (parkingSpaсe[i] == null) {
                freeSpaceForCars += 1;
            }
        }
        System.out.println("Свободных мест для машин: " + freeSpaceForCars);

        int freeSpaceForTracks = 0;
        for (int i = numberOfCars; i <= numberOfCars + numberOfTrucks - 1; i++) {
            if (parkingSpaсe[i] == null) {
                freeSpaceForTracks += 1;
            }
        }
        System.out.println("Свободных мест для грузовиков: " + freeSpaceForTracks);
        int minTimeForCars = 11;
        if (freeSpaceForCars == 0) {
            for (int i = 0; i <= numberOfCars - 1; i++) {
                if (parkingSpaсe[i] != null && parkingSpaсe[i].getTime() > 0) {
                    if (parkingSpaсe[i].getTime() < minTimeForCars) {
                        minTimeForCars = parkingSpaсe[i].getTime();
                    }
                }
            }
            System.out.println("Место для машины освободится через: " + minTimeForCars + "час");
        }
        int minTimeForTrucks = 11;
        if (freeSpaceForTracks == 0) {
            for (int i = numberOfCars; i <= numberOfCars + numberOfTrucks - 1; i++) {
                if (parkingSpaсe[i] != null && parkingSpaсe[i].getTime() > 0) {
                    if (parkingSpaсe[i].getTime() < minTimeForTrucks) {
                        minTimeForTrucks = parkingSpaсe[i].getTime();
                    }
                }
            }
            System.out.println("Место для грузовика освободится через: " + minTimeForTrucks + "час");
        }
    }

    private static void cleaningTheParkingLot() {
        for (int i = 0; i <= numberOfCars + numberOfTrucks - 1; i++) {
            if (parkingSpaсe[i] != null) {
                if (parkingSpaсe[i].getTime() > 0) {
                    parkingSpaсe[i].setTime(parkingSpaсe[i].getTime() - 1);
                }
                if (parkingSpaсe[i].getTime() == 0) {
                    parkingSpaсe[i] = null;
                }
            }
        }
    }

    private static void instruction() {
        System.out.println("Чтобы сделать 1 ход, введите: /s");
        System.out.println("Чтобы выйти из парковки, введите команду: /left");
    }

}

