public class Parking {
    private int numersOfCars;
    private int numbersOfTrucks;

    public Parking(int numersOfCars, int numbersOfTrucks) {
        this.numersOfCars = numersOfCars;
        this.numbersOfTrucks = numbersOfTrucks;
    }

    public int getNumersOfCars() {
        return numersOfCars;
    }

    public int getNumbersOfTrucks() {
        return numbersOfTrucks;
    }
}
